using System.Collections.Generic;
using System.Linq;

namespace PuzzleSolver
{
    public class PuzzleSolverState
    {
        public List<PuzzleBlock> FixedBlocks { get; }
        public List<PuzzleBlock> FloatingBlocks { get; }

        public bool IsFinal => FloatingBlocks.Count == 0;

        public bool IsSymmetric()
        {
            return new PuzzleBlock(FixedBlocks.SelectMany(block => block.Cells).ToList()).IsSymmetric();
        }

        public PuzzleSolverState(List<PuzzleBlock> blocks)
        {
            FloatingBlocks = blocks.Skip(1).ToList();
            FixedBlocks = blocks.Take(1).ToList();
        }

        private PuzzleSolverState(List<PuzzleBlock> fixedBlocks, List<PuzzleBlock> floatingBlocks)
        {
            FixedBlocks = fixedBlocks;
            FloatingBlocks = floatingBlocks;
        }

        public IEnumerable<PuzzleSolverState> ProduceStates()
        {
            foreach (var floatingBlock in FloatingBlocks)
            {
                foreach (var blockToPut in Enumerable
                    .Range(0, 4)
                    .Select(floatingBlock.RotateClockwise)
                    .Concat(new[] {floatingBlock.TurnOver()}))
                {
                    foreach (var translation in FixedBlocks
                        .SelectMany(block => block.CellsToContact())
                        .SelectMany(x => blockToPut.Cells.Select(y => (x, y))
                            .Select(pair => pair.Item1 - pair.Item2))
                        .Distinct())
                    {
                        var translatedBlock = blockToPut.Translate(translation);
                        if (FixedBlocks.Any(block => block.CollideWith(translatedBlock)))
                            continue;
                        yield return new PuzzleSolverState(
                            FixedBlocks.Concat(new[] {translatedBlock}).ToList(),
                            FloatingBlocks.Except(new[] {floatingBlock}).ToList()
                        );
                    }
                }
            }
        }
    }
}