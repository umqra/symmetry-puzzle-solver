using System;
using System.Collections.Generic;
using System.Linq;

namespace PuzzleSolver
{
    public class PuzzleSolver
    {
        public PuzzleBlock[] PuzzleBlocks { get; }

        public PuzzleSolver(PuzzleBlock[] puzzleBlocks)
        {
            PuzzleBlocks = puzzleBlocks;
        }

        public bool TrySolve(out List<PuzzleBlock> solution)
        {
            var initialState = new PuzzleSolverState(PuzzleBlocks.ToList());
            return TrySolve(initialState, out solution);
        }

        private static bool TrySolve(PuzzleSolverState state, out List<PuzzleBlock> solution)
        {
            solution = null;
            if (state.IsFinal)
            {
                if (!state.IsSymmetric())
                    return false;
                solution = state.FixedBlocks;
                return true;
            }

            foreach (var newState in state.ProduceStates())
            {
                if (TrySolve(newState, out solution))
                    return true;
            }

            return false;
        }
    }
}