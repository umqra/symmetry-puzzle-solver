﻿using System.Collections.Generic;
using System.Linq;

namespace PuzzleSolver
{
    public class PuzzleBlock
    {
        public PuzzleBlock(List<Cell> cells)
        {
            Cells = cells;
            cellsHashSet = new HashSet<Cell>(Cells);
        }

        public List<Cell> Cells { get; }

        private HashSet<Cell> cellsHashSet;

        public Cell LeftBottomCorner => Cells.OrderBy(x => x, Cell.ByCoordinatesComparerInstance).First();

        public Cell RightBottomCorner =>
            Cells.OrderBy(cell => new Cell(-cell.X, cell.Y), Cell.ByCoordinatesComparerInstance).First();

        public bool IsSymmetric()
        {
            return IsSymmetricByX() || RotateClockwise().IsSymmetricByX();
        }

        private bool IsSymmetricByX()
        {
            var doubledMiddleX = (LeftBottomCorner.X + RightBottomCorner.X);
            return Cells.All(cell => Contains(new Cell(doubledMiddleX - cell.X, cell.Y)));
        }

        public PuzzleBlock RotateClockwise()
        {
            return new PuzzleBlock(Cells.Select(cell => cell.RotateClockwise()).ToList());
        }

        public PuzzleBlock TurnOver()
        {
            return new PuzzleBlock(Cells.Select(cell => cell.TurnOver()).ToList());
        }

        public bool Contains(Cell cell)
        {
            return cellsHashSet.Contains(cell);
        }

        public PuzzleBlock Translate(Cell direction)
        {
            return new PuzzleBlock(Cells.Select(cell => cell + direction).ToList());
        }

        public bool CollideWith(PuzzleBlock block)
        {
            return Cells.Count < block.Cells.Count ? Cells.Any(block.Contains) : block.Cells.Any(Contains);
        }


        public override string ToString()
        {
            return string.Join(", ", Cells);
        }

        private const long FnwPrime = 16777619;
        private const long FnwOffsetBasis = 2166136261;

        private sealed class PuzzleBlockEqualityComparer : IEqualityComparer<PuzzleBlock>
        {
            public bool Equals(PuzzleBlock x, PuzzleBlock y)
            {
                var translation = y.LeftBottomCorner - x.LeftBottomCorner;
                return x.Cells
                    .Select(cell => cell + translation)
                    .OrderBy(cell => cell, Cell.ByCoordinatesComparerInstance)
                    .SequenceEqual(y.Cells.OrderBy(cell => cell, Cell.ByCoordinatesComparerInstance));
            }

            public int GetHashCode(PuzzleBlock block)
            {
                var corner = block.LeftBottomCorner;
                var normalizedCells = block.Cells
                    .Select(cell => cell - corner)
                    .OrderBy(x => x, Cell.ByCoordinatesComparerInstance);
                var hash = FnwOffsetBasis;
                foreach (var cell in normalizedCells)
                {
                    unchecked
                    {
                        hash ^= cell.GetHashCode();
                        hash *= FnwPrime;
                    }
                }

                return (int) hash;
            }
        }

        public static IEqualityComparer<PuzzleBlock> PuzzleBlockComparer { get; } = new PuzzleBlockEqualityComparer();
    }
}