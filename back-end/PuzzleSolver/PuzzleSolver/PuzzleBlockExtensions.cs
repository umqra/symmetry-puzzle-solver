using System.Collections.Generic;
using System.Linq;

namespace PuzzleSolver
{
    public static class PuzzleBlockExtensions
    {
        public static IEnumerable<Cell> CellsToContact(this PuzzleBlock block)
        {
            var reportedCells = new HashSet<Cell>();
            foreach (var cell in block.Cells)
            {
                foreach (var neighbour in cell.Neighbours)
                {
                    if (block.Contains(neighbour) || reportedCells.Contains(neighbour))
                        continue;
                    reportedCells.Add(neighbour);
                    yield return neighbour;
                }
            }
        }

        public static PuzzleBlock RotateClockwise(this PuzzleBlock block, int rotationsCount)
        {
            return Enumerable.Range(0, rotationsCount).Aggregate(block, (newBlock, _) => newBlock.RotateClockwise());
        }
    }
}