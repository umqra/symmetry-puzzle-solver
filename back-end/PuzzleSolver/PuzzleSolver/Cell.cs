using System.Collections.Generic;

namespace PuzzleSolver
{
    public class Cell
    {
        public Cell(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }

        public static Cell operator +(Cell first, Cell second)
        {
            return new Cell(first.X + second.X, first.Y + second.Y);
        }

        public static Cell operator -(Cell first, Cell second)
        {
            return new Cell(first.X - second.X, first.Y - second.Y);
        }

        public Cell TurnOver() => new Cell(-X, Y);

        public Cell RotateClockwise() => new Cell(Y, -X);

        protected bool Equals(Cell other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Cell) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Y;
            }
        }

        private sealed class ByCoordinatesComparer : IComparer<Cell>
        {
            public int Compare(Cell x, Cell y)
            {
                if (ReferenceEquals(x, y)) return 0;
                if (ReferenceEquals(null, y)) return 1;
                if (ReferenceEquals(null, x)) return -1;
                var xComparison = x.X.CompareTo(y.X);
                if (xComparison != 0) return xComparison;
                return x.Y.CompareTo(y.Y);
            }
        }

        public IEnumerable<Cell> Neighbours => new List<Cell>
        {
            this + new Cell(1, 0),
            this + new Cell(0, 1),
            this + new Cell(-1, 0),
            this + new Cell(0, -1)
        };

        public override string ToString()
        {
            return $"({X}, {Y})";
        }

        public static IComparer<Cell> ByCoordinatesComparerInstance { get; } = new ByCoordinatesComparer();
    }
}