using FluentAssertions;
using NUnit.Framework;
using PuzzleSolver;

namespace PuzzleSolverTests
{
    public class CellTests
    {
        [Test]
        public void TestPlus()
        {
            (new Cell(1, -2) + new Cell(-2, 3)).Should().Be(new Cell(-1, 1));
            (new Cell(1, -2) + new Cell(-2, 3)).Should().NotBe(new Cell(0, 1));
            (new Cell(1, -2) + new Cell(-2, 3)).Should().NotBe(new Cell(-1, 0));
        }

        [Test]
        public void TestMinus()
        {
            (new Cell(1, -2) - new Cell(-2, 3)).Should().Be(new Cell(3, -5));
            (new Cell(1, -2) - new Cell(-2, 3)).Should().NotBe(new Cell(3, 0));
            (new Cell(1, -2) - new Cell(-2, 3)).Should().NotBe(new Cell(0, -5));
        }

        [Test]
        public void TestClockwiseRotation()
        {
            new Cell(1, 0).RotateClockwise().Should().Be(new Cell(0, -1));
        }

        [Test]
        public void TestTurnover()
        {
            new Cell(1, 2).TurnOver().Should().Be(new Cell(-1, 2));
        }

        [Test]
        public void TestByCoordinateComparison()
        {
            Cell.ByCoordinatesComparerInstance.Compare(new Cell(0, -1), new Cell(0, 0)).Should()
                .Be(-1);
            Cell.ByCoordinatesComparerInstance.Compare(new Cell(0, 0), new Cell(0, 0)).Should().Be(0);
            Cell.ByCoordinatesComparerInstance.Compare(new Cell(0, 1), new Cell(0, 0)).Should().Be(1);
            Cell.ByCoordinatesComparerInstance.Compare(new Cell(-1, -1), new Cell(0, 0)).Should()
                .Be(-1);
            Cell.ByCoordinatesComparerInstance.Compare(new Cell(-1, 0), new Cell(0, 0)).Should()
                .Be(-1);
            Cell.ByCoordinatesComparerInstance.Compare(new Cell(-1, 1), new Cell(0, 0)).Should()
                .Be(-1);
            Cell.ByCoordinatesComparerInstance.Compare(new Cell(1, -1), new Cell(0, 0)).Should()
                .Be(1);
            Cell.ByCoordinatesComparerInstance.Compare(new Cell(1, 0), new Cell(0, 0)).Should().Be(1);
            Cell.ByCoordinatesComparerInstance.Compare(new Cell(1, 1), new Cell(0, 0)).Should().Be(1);
        }

        [Test]
        public void TestNeighbours()
        {
            new Cell(1, 2).Neighbours.Should()
                .BeEquivalentTo(new Cell(0, 2), new Cell(2, 2), new Cell(1, 1), new Cell(1, 3));
        }
    }
}