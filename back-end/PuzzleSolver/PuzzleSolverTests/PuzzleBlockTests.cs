using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using PuzzleSolver;

namespace PuzzleSolverTests
{
    public class PuzzleBlockTests
    {
        [Test]
        public void TestContains()
        {
            var figure = new PuzzleBlock(new List<Cell>
            {
                new Cell(0, 0),
                new Cell(1, 0),
                new Cell(1, 1)
            });
            figure.Contains(new Cell(0, 0)).Should().BeTrue();
            figure.Contains(new Cell(1, 0)).Should().BeTrue();
            figure.Contains(new Cell(1, 1)).Should().BeTrue();
            figure.Contains(new Cell(0, 1)).Should().BeFalse();
        }

        [Test]
        public void TestBlockEquality()
        {
            var a = CreateBlock(@"
.#
.#
.#
##");
            var a11 = CreateBlock(@"
...
..#
..#
..#
.##");
            var b = CreateBlock(@"
.#
.#
.#
.#
.#");
            a.Should().Match(x => PuzzleBlock.PuzzleBlockComparer.Equals((PuzzleBlock) x, a11));
            a.Should().Match(x => !PuzzleBlock.PuzzleBlockComparer.Equals((PuzzleBlock) x, b));
        }

        [Test]
        public void TestRotation()
        {
            var original = CreateBlock(@"
.#
.#
.#
##");
            var expected = CreateBlock(@"
#...
####");
            original.RotateClockwise().Should()
                .Match(x => PuzzleBlock.PuzzleBlockComparer.Equals((PuzzleBlock) x, expected));
        }

        [Test]
        public void TestTurnOver()
        {
            var original = CreateBlock(@"
.#
.#
.#
##");
            var expected = CreateBlock(@"
.#
.#
.#
.##");
            ;
            original.TurnOver().Should().Match(x => PuzzleBlock.PuzzleBlockComparer.Equals((PuzzleBlock) x, expected));
        }

        [Test]
        public void TestSymmetricByX()
        {
            var figure = CreateBlock(@"
..###.
...#..
...#..
.#.#.#
.#####");
            figure.IsSymmetric().Should().BeTrue();
        }

        [Test]
        public void TestSymmetricByY()
        {
            var figure = CreateBlock(@"
.#####
.#.#..
...#..
.#.#..
.#####");
            figure.IsSymmetric().Should().BeTrue();
        }

        [Test]
        public void TestNotSymmetric()
        {
            var figure = CreateBlock(@"
.#####
...#..
...#..
.#.#..
.#####");
            figure.IsSymmetric().Should().BeFalse();
        }

        [Test]
        public void TestCellsToContact()
        {
            var figure = new PuzzleBlock(new List<Cell>
            {
                new Cell(0, 1),
                new Cell(0, 0),
                new Cell(1, 0),
                new Cell(2, 0),
                new Cell(2, 1),
            });
            figure.CellsToContact().Should().BeEquivalentTo(
                new Cell(-1, 0),
                new Cell(-1, 1),
                new Cell(0, 2),
                new Cell(1, 1),
                new Cell(2, 2),
                new Cell(3, 1),
                new Cell(3, 0),
                new Cell(2, -1),
                new Cell(1, -1),
                new Cell(0, -1));
        }

        private static PuzzleBlock CreateBlock(string line)
        {
            var cells = new List<Cell>();
            var lines = line.Trim().Split();
            for (var y = 0; y < lines.Length; y++)
            {
                for (var x = 0; x < lines[y].Length; x++)
                {
                    if (lines[y][x] == '#')
                    {
                        cells.Add(new Cell(x, lines.Length - y - 1));
                    }
                }
            }

            return new PuzzleBlock(cells);
        }
    }
}