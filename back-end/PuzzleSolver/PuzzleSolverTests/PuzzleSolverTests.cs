using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using PuzzleSolver;

namespace PuzzleSolverTests
{
    public class PuzzleSolverTests
    {
        [Test]
        public void TestSimplePuzzle()
        {
            var result = new PuzzleSolver.PuzzleSolver(new[]
            {
                new PuzzleBlock(new List<Cell> {new Cell(0, 0)}),
                new PuzzleBlock(new List<Cell> {new Cell(0, 0)}),
                new PuzzleBlock(new List<Cell> {new Cell(0, 0)}),
                new PuzzleBlock(new List<Cell> {new Cell(0, 0)}),
            }).TrySolve(out var solution);
            result.Should().BeTrue();
            ValidateSolution(solution);
        }

        [Test]
        public void TestPuzzleWithoutSolution()
        {
            var result = new PuzzleSolver.PuzzleSolver(new[]
            {
                new PuzzleBlock(new List<Cell>
                {
                    new Cell(0, 0),
                    new Cell(1, 0),
                    new Cell(2, 0),
                    new Cell(3, 0),
                    new Cell(3, 1),
                    new Cell(3, 2),
                    new Cell(3, 3),
                }),
                new PuzzleBlock(new List<Cell>
                {
                    new Cell(0, 0),
                    new Cell(1, 0),
                }),
            }).TrySolve(out var solution);
            result.Should().BeFalse();
            solution.Should().BeNull();
        }

        [Test]
        public void TestInterestingPuzzle()
        {
            var result = new PuzzleSolver.PuzzleSolver(new[]
            {
                new PuzzleBlock(new List<Cell>
                {
                    new Cell(0, 0),
                    new Cell(0, 1),
                    new Cell(0, 2),
                    new Cell(1, 2)
                }),
                new PuzzleBlock(new List<Cell>
                {
                    new Cell(0, 0),
                    new Cell(1, 0),
                    new Cell(2, 0),
                    new Cell(1, 1)
                }),
            }).TrySolve(out var solution);
            result.Should().BeTrue();
            ValidateSolution(solution);
        }

        [Test]
        public void TestLargePuzzle()
        {
            var result = new PuzzleSolver.PuzzleSolver(new[]
            {
                new PuzzleBlock(new List<Cell>
                {
                    new Cell(0, 0),
                    new Cell(1, 0),
                    new Cell(2, 0),
                    new Cell(2, 1)
                }),
                new PuzzleBlock(new List<Cell>
                {
                    new Cell(0, 0),
                    new Cell(1, 0),
                    new Cell(2, 0),
                    new Cell(3, 0)
                }),
                new PuzzleBlock(new List<Cell>
                {
                    new Cell(0, 0),
                    new Cell(1, 0),
                    new Cell(1, 1),
                    new Cell(0, 1)
                }),
                new PuzzleBlock(new List<Cell>
                {
                    new Cell(0, 0),
                    new Cell(1, 0),
                    new Cell(2, 0),
                    new Cell(1, 1)
                }),
                new PuzzleBlock(new List<Cell>
                {
                    new Cell(0, 1),
                    new Cell(1, 1),
                    new Cell(1, 0),
                    new Cell(2, 0)
                }),
            }).TrySolve(out var solution);
            result.Should().BeTrue();
            ValidateSolution(solution);
        }

        [Test]
        public void StackOverflowDuringTestingTest()
        {
            var result = new PuzzleSolver.PuzzleSolver(new[]
            {
                new PuzzleBlock(new List<Cell>
                {
                    new Cell(1, 1),
                    new Cell(2, 1),
                    new Cell(3, 1),
                    new Cell(1, 2)
                }),
                new PuzzleBlock(new List<Cell>
                {
                    new Cell(3, 1),
                    new Cell(3, 3),
                    new Cell(2, 2),
                    new Cell(3, 2)
                }),
            }).TrySolve(out var solution);
            result.Should().BeTrue();
            ValidateSolution(solution);
        }

        private void ValidateSolution(List<PuzzleBlock> solution)
        {
            foreach (var first in solution)
            foreach (var second in solution)
            {
                if (ReferenceEquals(first, second))
                    continue;
                first.CollideWith(second).Should().BeFalse();
            }

            foreach (var block in solution)
                Console.WriteLine(block);
            new PuzzleBlock(solution.SelectMany(x => x.Cells).ToList()).IsSymmetric().Should().BeTrue();
        }
    }
}