using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PuzzleSolver;

namespace PuzzleSolverApi
{
    [Route("api")]
    [ApiController]
    public class PuzzleSolverApiController : Controller
    {
        [HttpPost("solve")]
        public JsonResult SolvePuzzle([FromBody] PuzzleModel puzzle)
        {
            var puzzleBlocks = puzzle.Blocks.Select(block =>
                new PuzzleBlock(block.Cells.Select(cell => new Cell(cell.X, cell.Y)).ToList())).ToArray();
            foreach (var block in puzzleBlocks)
                Console.WriteLine(block);
            var result = new PuzzleSolver.PuzzleSolver(puzzleBlocks).TrySolve(out var solution);
            return Json(result
                ? solution.Select(block => new PuzzleBlockModel
                {
                    Cells = block.Cells.Select(cell => new CellModel {X = cell.X, Y = cell.Y}).ToArray()
                }).ToList()
                : new List<PuzzleBlockModel>());
        }
    }
}