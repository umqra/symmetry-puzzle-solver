namespace PuzzleSolverApi
{
    public class CellModel
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}