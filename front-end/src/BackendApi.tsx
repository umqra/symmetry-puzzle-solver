import axios from "axios";
import * as React from "react";
import {Block} from "./Block";

export interface IBackendApi {
    solvePuzzle(blocks: Block[]): Promise<Block[]>;
}

export default class BackendApi implements IBackendApi {
    private serverAddress: string;

    constructor(serverAddress) {
        this.serverAddress = serverAddress;
    }

    public async solvePuzzle(blocks: Block[]): Promise<Block[]> {
        const response = await this.makePostRequest('solve', {
            blocks: blocks
        });
        return response.data;
    }

    public async makeGetRequest(path: string): Promise<any> {
        return await axios.get(`http://${this.serverAddress}/api/${path}`);
    }

    public async makePostRequest(path: string, data: any): Promise<any> {
        return await axios.post(`http://${this.serverAddress}/api/${path}`, data);
    }
}
