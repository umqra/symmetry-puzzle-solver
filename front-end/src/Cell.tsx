export interface Cell {
    x: number;
    y: number;
    color?: string;
}