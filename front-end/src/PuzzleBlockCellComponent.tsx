import * as React from "react";
import styled, {css} from "styled-components";

interface StyledCellComponentProps {
    size: number;
    isReadOnly?: boolean;
    used: boolean;
    color?: string;
}

const CellComponent = styled.div`
  border: 1px solid black;
  margin-top: -1px;
  margin-left: -1px;
  width: ${(p: StyledCellComponentProps) => p.size}em;
  height: ${(p: StyledCellComponentProps) => p.size}em;
  background-color: ${(p: StyledCellComponentProps) => p.used ? (p.color == undefined ? "cornflowerblue" : p.color) : "white"};
  ${(p: StyledCellComponentProps) => p.isReadOnly !== true && css`
  &:hover {
    background-color: lightblue;
  }
  &:active {
    background-color: lightskyblue;
  }`};
`;

interface PuzzleBlockCellComponentProps {
    state: CellState;
    size: number;
    onChange: (state: CellState) => void;
    isReadOnly?: boolean;
}

export interface CellState {
    used: boolean;
    color?: string;
}

export class PuzzleBlockCellComponent extends React.Component<PuzzleBlockCellComponentProps> {
    public render(): React.ReactNode {
        const {state, onChange, ...rest} = this.props;
        return <CellComponent {...rest} {...state} onClick={(e) => onChange({used: !state.used, color: state.color})}/>
    }
}