import * as React from "react";
import * as _ from "lodash";
import {Cell} from "./Cell";
import {PuzzleBlockRowComponent} from "./PuzzleBlockRowComponent";
import styled from "styled-components";

interface PuzzleBlockComponentProps {
    cells: Cell[];
    size: number;
    widthCells: number;
    heightCells: number;
    onChange: (cells: Cell[]) => void;
    isReadOnly?: boolean;
}

const PuzzleBlockWrapper = styled.div`
  margin: 10px;
  display: inline-block;
`;

export class PuzzleBlockComponent extends React.Component<PuzzleBlockComponentProps> {
    public render(): React.ReactNode {
        const {cells, widthCells, heightCells, onChange, ...rest} = this.props;
        const yCoords = [...Array(heightCells)].map((_, i) => i);
        const xCoords = [...Array(widthCells)].map((_, i) => i);
        return (
            <PuzzleBlockWrapper>
                {yCoords.map(y => <PuzzleBlockRowComponent
                    key={y}
                    states={xCoords.map(x => {
                        const found = _.find(cells, {x: x, y: y});
                        if (found === undefined) {
                            return {used: false};
                        }
                        return {used: true, color: found.color};
                    })}
                    onChange={(row) => onChange(cells
                        .filter(cell => cell.y != y)
                        .concat(row.map((value, i) => ({
                            x: i,
                            y: y,
                            value: value.used,
                            color: value.color,
                        })).filter(cell => cell.value))
                        .map(cell => ({x: cell.x, y: cell.y})))}
                    {...rest}
                />)}
            </PuzzleBlockWrapper>
        );
    }
}

interface DeletablePuzzleBlockComponentProps extends PuzzleBlockComponentProps {
    onDelete?: () => void;
}

export class DeletablePuzzleBlockComponent extends React.Component<DeletablePuzzleBlockComponentProps> {
    public render(): React.ReactNode {
        const {onDelete, ...rest} = this.props;
        return (
            <PuzzleBlockWrapper>
                <div>
                    <PuzzleBlockComponent {...rest}/>
                    {onDelete != undefined &&
                    <div>
                        <button onClick={onDelete ? (e) => onDelete() : undefined}>Delete</button>
                    </div>
                    }
                </div>
            </PuzzleBlockWrapper>
        );
    }
}