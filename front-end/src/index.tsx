import * as React from "react";
import * as ReactDOM from "react-dom";
import {HashRouter} from "react-router-dom";
import BackendApi from "./BackendApi";
import Application from "./Application";

const backendApi = new BackendApi("192.241.154.146:5000");

ReactDOM.render(
    <HashRouter>
        <Application backendApi={backendApi}/>
    </HashRouter>,
    document.getElementById("app"),
);
