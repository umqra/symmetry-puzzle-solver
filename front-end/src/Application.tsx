import * as React from "react";
import {withRouter} from "react-router";
import {IBackendApi} from "./BackendApi";
import {PuzzleBlockComponent} from "./PuzzleBlockComponent";
import PuzzleEditorComponent from "./PuzzleEditorComponent";
import {Block} from "./Block";
import {PuzzleBlockListComponent} from "./PuzzleBlockListComponent";
import {Cell} from "./Cell";
import styled from "styled-components";
import * as _ from "lodash";


interface IApplicationProps {
    backendApi: IBackendApi;
}

interface IApplicationState {
    blocks: Block[];
    solution: Block[];
    isSolving: boolean;
}

const ApplicationStyle = styled.div`
  text-align: center;
`;

class Application extends React.Component<IApplicationProps> {
    state = {
        blocks: [],
        solution: [],
        isSolving: false,
    };

    public render(): React.ReactNode {
        return (
            <ApplicationStyle>
                {this.renderInternal()}
            </ApplicationStyle>
        );
    }

    private renderInternal(): React.ReactNode {
        const {backendApi} = this.props;
        const {blocks, isSolving} = this.state;

        if (blocks.length === 0) {
            return (
                <div>
                    {isSolving && <h3>Puzzle is solving...</h3>}
                    {!isSolving && <h3>Configure puzzle to solve</h3>}
                    <PuzzleEditorComponent isReadOnly={isSolving}
                                           widthCells={8}
                                           heightCells={8}
                                           size={1.5}
                                           onSolve={async (blocks) => {
                                               await this.setState({isSolving: true});
                                               const solution = await backendApi.solvePuzzle(blocks);
                                               this.setState({
                                                   blocks: blocks,
                                                   solution: solution,
                                                   isSolving: false,
                                               });
                                           }}/>
                </div>);
        }
        const {cells, widthCells, heightCells} = this.flattenSolution();
        return (
            <div>
                <h3>Puzzle solution</h3>
                <PuzzleBlockComponent isReadOnly
                                      cells={cells}
                                      size={1.5}
                                      widthCells={widthCells}
                                      heightCells={heightCells}
                                      onChange={(x) => {
                                      }}
                />
                <PuzzleBlockListComponent widthCells={8}
                                          heightCells={8}
                                          size={0.5}
                                          blocks={blocks}
                />
                <button onClick={(e) => this.setState({
                    blocks: [],
                    solution: [],
                    isSolving: false
                })}>
                    Clear
                </button>
            </div>
        );
    }

    private flattenSolution(): { cells: Cell[], widthCells: number, heightCells: number } {
        const {solution} = this.state;
        const colors = [
            "orange",
            "green",
            "gray",
            "blue",
            "yellow",
            "red",
        ];
        let result = Array();
        let xCoords = [];
        let yCoords = [];
        for (let blockId = 0; blockId < solution.length; blockId++) {
            xCoords = xCoords.concat(solution[blockId].cells.map(cell => cell.x));
            yCoords = yCoords.concat(solution[blockId].cells.map(cell => cell.y));
        }
        console.log(xCoords);
        console.log(yCoords);
        const [minX, maxX] = _.over(Math.min, Math.max)(...xCoords);
        const [minY, maxY] = _.over(Math.min, Math.max)(...yCoords);
        console.log(minX, maxX, minY, maxY);
        for (let blockId = 0; blockId < solution.length; blockId++) {
            result = result.concat(solution[blockId].cells.map(cell => ({
                x: cell.x - minX,
                y: cell.y - minY,
                color: colors[blockId % colors.length],
            })));
        }
        return {
            cells: result,
            widthCells: maxX - minX + 1,
            heightCells: maxY - minY + 1,
        };
    }
}

// @ts-ignore
export default withRouter(Application);
