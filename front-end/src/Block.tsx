import {Cell} from "./Cell";

export interface Block {
    cells: Cell[];
}