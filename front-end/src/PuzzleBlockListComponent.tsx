import * as React from "react";
import {DeletablePuzzleBlockComponent} from "./PuzzleBlockComponent";
import styled from "styled-components";
import {Block} from "./Block";

const PuzzlesRow = styled.div`
  display: inline-block;
  width: 100%;
`;

interface PuzzleBlockListComponentProps {
    widthCells: number;
    heightCells: number;
    size: number;
    blocks: Block[];
    onChange?: (blocks: Block[]) => void;
}

export class PuzzleBlockListComponent extends React.Component<PuzzleBlockListComponentProps> {
    public render(): React.ReactNode {
        const {blocks, onChange, ...rest} = this.props;
        return (
            <PuzzlesRow>
                {blocks.map((block, i) =>
                    <DeletablePuzzleBlockComponent
                        onDelete={onChange ? () => onChange([].concat(blocks.slice(0, i), blocks.slice(i + 1))) : undefined}
                        isReadOnly={true}
                        key={i}
                        cells={block.cells}
                        onChange={cells => {
                        }}
                        {...rest}
                    />)}
            </PuzzlesRow>
        );
    }
}