import * as React from "react";
import {Block} from "./Block";
import PuzzleBlockEditorComponent from "./PuzzleBlockEditorComponent";
import {PuzzleBlockListComponent} from "./PuzzleBlockListComponent";

interface PuzzleEditorComponentProps {
    widthCells: number;
    heightCells: number;
    size: number;
    onSolve: (blocks: Block[]) => void;
    isReadOnly?: boolean;
}

interface PuzzleEditorComponentState {
    blocks: Block[];
}

export default class PuzzleEditorComponent extends React.Component<PuzzleEditorComponentProps, PuzzleEditorComponentState> {
    state = {
        blocks: []
    };

    public render(): React.ReactNode {
        const {blocks} = this.state;
        const {onSolve, size, widthCells, heightCells, ...rest} = this.props;
        return (
            <div>
                <PuzzleBlockEditorComponent {...rest}
                                            widthCells={widthCells}
                                            heightCells={heightCells}
                                            size={size}
                                            onSubmit={(cells) => {
                                                this.setState({
                                                    blocks: blocks.concat([{cells: cells}])
                                                })
                                            }}/>

                <PuzzleBlockListComponent
                    onChange={(blocks) => this.setState({blocks: blocks})}
                    heightCells={heightCells}
                    widthCells={widthCells}
                    size={0.5}
                    blocks={blocks}
                />

                <button disabled={blocks.length == 0} onClick={(e) => onSolve(blocks)}>
                    Solve
                </button>
            </div>
        );
    }
}