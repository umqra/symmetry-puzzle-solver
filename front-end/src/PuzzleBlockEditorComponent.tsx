import * as React from "react";
import {Cell} from "./Cell";
import {PuzzleBlockComponent} from "./PuzzleBlockComponent";

interface PuzzleBlockEditorComponentProps {
    widthCells: number;
    heightCells: number;
    size: number;
    onSubmit: (cells: Cell[]) => void;
    isReadOnly?: boolean;
}

interface PuzzleBlockEditorComponentState {
    cells: Cell[];
}

export default class PuzzleBlockEditorComponent extends React.Component<PuzzleBlockEditorComponentProps, PuzzleBlockEditorComponentState> {
    state = {
        cells: []
    };

    public render(): React.ReactNode {
        const {cells} = this.state;
        const {onSubmit, isReadOnly, ...rest} = this.props;
        return (
            <div>
                <PuzzleBlockComponent cells={cells}
                                      onChange={cells => this.setState({cells: cells})}
                                      {...rest}/>
                <div>
                    <button disabled={cells.length == 0 || isReadOnly} onClick={(e) => {
                        onSubmit(cells);
                        this.setState({cells: []});
                    }}>
                        Add
                    </button>
                </div>
            </div>
        );
    }
}