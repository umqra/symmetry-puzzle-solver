import * as React from "react";
import {CellState, PuzzleBlockCellComponent} from "./PuzzleBlockCellComponent";
import styled from "styled-components";

const CellsRow = styled.div`
  display: inline-block;
`;

interface PuzzleBlockRowComponentProps {
    states: CellState[];
    size: number;
    onChange: (used: CellState[]) => void;
    isReadOnly?: boolean;
}

export class PuzzleBlockRowComponent extends React.Component<PuzzleBlockRowComponentProps> {
    public render(): React.ReactNode {
        const {states, onChange, ...rest} = this.props;
        return (
            <CellsRow>
                {states.map((x, i) => <PuzzleBlockCellComponent
                    key={i}
                    state={x}
                    onChange={value => onChange([].concat(states.slice(0, i), [value], states.slice(i + 1)))}
                    {...rest}/>
                )}
            </CellsRow>
        );
    }
}