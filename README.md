## Simple web-service to solve symmetric puzzles

I am really bad at solving puzzles of any sort but at least I can write some code that do it for me

There is a simple web service that allow you to configure tiles of puzzle and then trying to solve it.

Made with [Docker](https://www.docker.com/), [.NET Core](https://dotnet.microsoft.com/download), [NUnit](https://nunit.org/), [pnpm](https://pnpm.js.org/), [typescript](https://www.typescriptlang.org/), [react](https://reactjs.org/), [styled-components](https://www.styled-components.com/) and [webpack](https://webpack.js.org/)

![See puzzle solution](images/solution.jpg)
